/**
 * Created by Mad on 0003, 03-11-16.
 */

import javax.mail.*;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class EmerchHandler {

    private final String propFileName = "src/main/resources/credentials/EmailConfig.properties";
    private static EmerchHandler instance = null;
    private Session session;
    private Store store;
    private Folder folder;
    private Message message[];
    private List<File> attachments = new ArrayList<File>();
    private Properties sessionProperties;
    private PropertiesContainer emailData = new PropertiesContainer(propFileName);
    private ArrayList<ParserDataContainer> parserResults = new ArrayList();
    private ArrayList<EmerchDataContainer> eMerchImports = new ArrayList();

//    private static final String PARSER_MAIL_PATTERN_1 = "^\\s*SUMMARY\\s*(.*)";
//    private static final String PARSER_MAIL_PATTERN_2 = "\\s*ERRORS\\^";

    public EmerchHandler() throws MessagingException, IOException  {
        try {
            InputStream inputStream = EmerchHandler.class.getResourceAsStream("src/main/resources/util/EmailConfig.properties");
            sessionProperties = new Properties();
            sessionProperties.load(inputStream);
            inputStream.close();
            session = Session.getInstance(sessionProperties);

            store = session.getStore("imaps");
            store.connect(emailData.getVar("host"), emailData.getVar("user"), emailData.getVar("password"));
            folder = store.getFolder("Emerch");
            folder.open(Folder.READ_ONLY);
            message = folder.getMessages();

            for (int i = 0; i < 1; i++) {
                if (message[i].getSubject().equals("NAB Transact - eMerch export file")) {

                    Multipart multipart = (Multipart) message[i].getContent();
                    BodyPart bodyPart = multipart.getBodyPart(1);

                    InputStream attachmentInput = bodyPart.getInputStream();
                    File attachment = new File(bodyPart.getFileName());
                    FileOutputStream attachmentOutput = new FileOutputStream(attachment);
                    byte[] buffer = new byte[4096];
                    int bytesRead;
                    while ((bytesRead = attachmentInput.read(buffer)) != -1) {
                        attachmentOutput.write(buffer, 0, bytesRead);
                    }
                    attachmentOutput.close();

                    BufferedReader br = new BufferedReader(new FileReader(attachment));
                    String line = null;
                    while ((line = br.readLine()) != null) {
                        //                        System.out.println(line);
                        EmerchDataContainer eMerchData = new EmerchDataContainer(line);
                        eMerchImports.add(eMerchData);
                    }
                }

                if (message[i].getSubject().equals("NAB Transact : eMerch Signup Parser : SUCCESS") ||
                        message[i].getSubject().equals("NAB Transact : eMerch Signup Parser : FAILURE")) {

                    Matcher matcher = Pattern.compile("(?<=Customer Number[\r\n])(.*)(?= ERRORS)", Pattern.DOTALL)
                                             .matcher(message[i].getContent().toString());
                    while (matcher.find()) {
                        //                        int parserResultsIndex = 0;
                        String[] lines = matcher.group(1).split("\n");

                        for (int ii = 0; ii < lines.length; ii++) {
                            if (lines[ii].length() > 64 && !lines[ii].contains("-----")) {

                                String clientID = lines[ii].substring(56, 64);
                                String ebNo = lines[ii].substring(0, 5);
                                ParserDataContainer parserData = new ParserDataContainer(clientID, ebNo);
                                parserResults.add(parserData);
                            }
                        }
                    }
                }
            }

            for (int i = 0; i < eMerchImports.size() - 1; i++) {
                for (int ii = 0; ii < parserResults.size() - 1; ii++) {
                    if (eMerchImports.get(i).getEbNo().equals(parserResults.get(ii).getEbNo())) {
                        eMerchImports.get(i).setClientID(parserResults.get(ii).getClientID());
                    }
                }
            }

            for (int i = 0; i < eMerchImports.size() - 1; i++) {
                System.out.println(eMerchImports.get(i).eMerchToString());
            }

        } catch (Exception e) {
            System.out.println(e);
        }
    }

    public static String FormatMailTimeStamp(String nativeDate) {
        //EEE MMM dd HH:mm:ss z yyyy
        // to
        //yyyyMMddHHmmss

        SimpleDateFormat dateFormat = new SimpleDateFormat("EEE MMM dd HH:mm:ss z yyyy");
        try {
            Date newDate = dateFormat.parse(nativeDate);
            dateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
            return dateFormat.format(newDate);
        } catch (Exception e) {
            System.out.println(e);
            return null;
        }
    }

//    public static String ReadLastTimeStamp() throws IOException {
//        String filePath = "util/LastTimeStamp.txt";
//        byte[] encoded = Files.readAllBytes(Paths.get(filePath));
//        return new String(encoded);
//    }

    public static void UpdateLastTimeStamp() {

    }

    public static EmerchHandler getInstance() throws MessagingException, IOException {
        if (instance == null) {
            instance = new EmerchHandler();
        }
        return instance;
    }

    public Properties getMailProperties() {
        return sessionProperties;
    }

    public Session getEmailSession() {
        return session;
    }

    public void sendEmail(Message email) throws MessagingException, Exception {
        Transport.send(email);
    }

}
