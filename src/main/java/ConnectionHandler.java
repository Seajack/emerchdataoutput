import javax.mail.*;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

/**
 * Created by Sean Ackerley on 0008, 08-05-17.
 */
public class ConnectionHandler {
    private final String propFileName = "credentials/EmailConfig.properties";
    private Session session;
    private Store store;
    private Folder fromFolder;
    private Message message[];
    private List<File> attachments = new ArrayList<File>();
    private Properties sessionProperties = new Properties();
    private PropertiesContainer emailData = new PropertiesContainer(propFileName);

    public ConnectionHandler() throws MessagingException, IOException {
//        InputStream inputStream = getClass().getClassLoader().getResourceAsStream(propFileName);
//        sessionProperties.load(inputStream);
//        sessionProperties = Session.getDefaultInstance(sessionProperties,null);
//        sessionProperties.load(inputStream);
//        inputStream.close();
        session = Session.getInstance(sessionProperties, null);

//        session = Session.getDefaultInstance(sessionProperties, null);
        sessionProperties.put("mail.store.protocol", "imaps");
        sessionProperties.put("mail.imaps.host", emailData.getVar("host"));
        sessionProperties.put("mail.imaps.port", "993");


        store = session.getStore("imaps");
        store.connect(emailData.getVar("host"), emailData.getVar("user"), emailData.getVar("password"));

        fromFolder = store.getFolder("Conveyor");
        fromFolder.open(Folder.READ_WRITE);
        message = fromFolder.getMessages();
    }

    public Message[] getMessage() {
        return this.message;
    }

    public Folder getFromFolder() {
        return this.fromFolder;
    }

}
