import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

public class Main {

    public static void main(String[] args) {
        try {
            InitializeHandler initializer = new InitializeHandler();
            initializer.Initialize();
            ConsumptionHandler consumption = new ConsumptionHandler(initializer.getDataContainer());
            consumption.HandleExistingData();
            consumption.ConsumeMessages();

        } catch (Exception e) {
            try {
                LogException(e);
            } catch (IOException ie) {
                ie.printStackTrace();
            }
        }
    }


    public static void LogException(Exception e) throws IOException {
        FileWriter fWriter = new FileWriter("src/main/resources/util/importlog.txt", true);
        PrintWriter pWriter = new PrintWriter(fWriter);
        e.printStackTrace(pWriter);
        pWriter.close();
        fWriter.close();
    }
}
