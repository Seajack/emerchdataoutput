/**
 * Created by Mad on 0003, 03-11-16.
 */

import javax.mail.*;
import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ConsumptionHandler {

    private static EmerchHandler instance = null;
    private Session session;
    private Store store;
    private Folder folder;
    private Message message[];
    private List<File> attachments = new ArrayList<File>();
    private Properties sessionProperties;
    private ArrayList<ParserDataContainer> parserResults = new ArrayList();
    private ArrayList<EmerchDataContainer> eMerchImports = new ArrayList();
    private ConnectionHandler connection;
    private DataContainer dataContainer;

//    private static final String PARSER_MAIL_PATTERN_1 = "^\\s*SUMMARY\\s*(.*)";
//    private static final String PARSER_MAIL_PATTERN_2 = "\\s*ERRORS\\^";

    public ConsumptionHandler(DataContainer container) throws MessagingException, IOException {
        connection = new ConnectionHandler();
        message = connection.getFromFolder().getMessages();
        dataContainer = container;
    }

    public void HandleExistingData() {
        parserResults = dataContainer.getParserResults();
        eMerchImports = dataContainer.geteMerchImports();
    }

    public void ConsumeMessages() throws MessagingException, IOException {
        int count = connection.getMessage().length;
        for (int i = 0; i < count; i++) {
            if (message[i].getSubject().equals("NAB Transact - eMerch export file")) {
                ConsumeExport(message[i]);
                FlagDelete(message[i]);
            }
            if (message[i].getSubject().equals("NAB Transact : eMerch Signup Parser : SUCCESS") ||
                    message[i].getSubject().equals("NAB Transact : eMerch Signup Parser : FAILURE")) {
                ConsumeParser(message[i]);
                FlagDelete(message[i]);
            } else {
                FlagDelete(message[i]);
            }
        }
        CollateImportData();
        connection.getFromFolder().expunge();
    }

    public void ConsumeExport(Message message) throws MessagingException, IOException {
        Multipart multipart = (Multipart) message.getContent();
        BodyPart bodyPart = multipart.getBodyPart(1);

        InputStream attachmentInput = bodyPart.getInputStream();
        File attachment = new File(bodyPart.getFileName());
        FileOutputStream attachmentOutput = new FileOutputStream(attachment);
        byte[] buffer = new byte[4096];
        int bytesRead;
        while ((bytesRead = attachmentInput.read(buffer)) != -1) {
            attachmentOutput.write(buffer, 0, bytesRead);
        }
        attachmentOutput.close();

        BufferedReader br = new BufferedReader(new FileReader(attachment));
        String line = null;
        while ((line = br.readLine()) != null) {
            EmerchDataContainer eMerchData = new EmerchDataContainer(line);
            eMerchImports.add(eMerchData);
        }
    }

    public void ConsumeParser(Message message) throws MessagingException, IOException {
        Matcher matcher = Pattern.compile("(?<=Customer Number[\r\n])(.*)(?= ERRORS)", Pattern.DOTALL)
                                 .matcher(message.getContent().toString());
        while (matcher.find()) {
            String[] lines = matcher.group(1).split("\n");

            for (int i = 0; i < lines.length; i++) {
                if (lines[i].length() > 64 && !lines[i].contains("-----")) {

                    String clientID = lines[i].substring(56, 64);
                    String ebNo = lines[i].substring(0, 5);
                    ParserDataContainer parserData = new ParserDataContainer(clientID, ebNo);
                    parserResults.add(parserData);
                }
            }
        }
    }

    public void FlagDelete(Message message) throws MessagingException {
        message.setFlag(Flags.Flag.DELETED, true);
    }

    public void CollateImportData() throws IOException {
        for (int i = 0; i < eMerchImports.size(); i++) {
            for (int ii = 0; ii < parserResults.size(); ii++) {
                if (eMerchImports.get(i).getEbNo().equals(parserResults.get(ii).getEbNo())) {
                    String compareEMerch = eMerchImports.get(i).eMerchToString();
                    String compareParser = parserResults.get(ii).parserToString();
                    eMerchImports.get(i).setClientID(parserResults.get(ii).getClientID());


                    FileReader fReader = new FileReader("src/main/resources/util/eMerchRegister.txt");
                    BufferedReader bReader = new BufferedReader(fReader);

                    String line;
                    Boolean match = false;
                    while ((line = bReader.readLine()) != null) {
                        if (line.equals(eMerchImports.get(i).eMerchToString())) {
                            match = true;
                        }
                    }
                    bReader.close();

                    if (!match) {
                        File registerFile = new File("src/main/resources/util/eMerchRegister.txt");
                        FileWriter registerWriter = new FileWriter(registerFile.getAbsoluteFile(), true);
                        BufferedWriter registerBuffer = new BufferedWriter(registerWriter);
                        registerBuffer.write(eMerchImports.get(i).eMerchToString() + "\n");
                        registerBuffer.close();
                        registerWriter.close();

                        File eMerchFile = new File("src/main/resources/util/eMerchExports.txt");
                        String eMerchOverwrite = "";
                        BufferedReader eMerchRead = new BufferedReader(new FileReader(eMerchFile));
                        String eMerchLine;
                        while ((eMerchLine = eMerchRead.readLine()) != null) {
                            if (!eMerchLine.equals(compareEMerch)) {
                                eMerchOverwrite = eMerchOverwrite + eMerchLine + "\n";
                            }
                        }
                        eMerchRead.close();
                        FileWriter eMerchWriter = new FileWriter("src/main/resources/util/eMerchExports.txt", false);
                        eMerchWriter.write(eMerchOverwrite);
                        eMerchWriter.close();


                        File parserFile = new File("src/main/resources/util/ParserResults.txt");
                        String parserOverwrite = "";
                        BufferedReader parserRead = new BufferedReader(new FileReader(parserFile));
                        String parserLine;
                        while ((parserLine = parserRead.readLine()) != null) {
                            if (!parserLine.equals(compareParser)) {
                                parserOverwrite = parserOverwrite + parserLine + "\n";
                            }
                        }
                        parserRead.close();
                        FileWriter parserWriter = new FileWriter("src/main/resources/util/ParserResults.txt", false);
                        parserWriter.write(parserOverwrite);
                        parserWriter.close();
                    }
                    parserResults.remove(ii);
                }
            }

            if (eMerchImports.get(i).getClientID().equals("")) {
                FileReader fReader = new FileReader("src/main/resources/util/eMerchExports.txt");
                BufferedReader bReader = new BufferedReader(fReader);

                String line;
                Boolean match = false;
                while ((line = bReader.readLine()) != null) {
                    if (line.equals(eMerchImports.get(i).eMerchToString())) {
                        match = true;
                    }
                }
                bReader.close();

                if (!match) {
                    File file = new File("src/main/resources/util/eMerchExports.txt");
                    FileWriter fWriter = new FileWriter(file.getAbsoluteFile(), true);
                    BufferedWriter bWriter = new BufferedWriter(fWriter);
                    bWriter.write(eMerchImports.get(i).eMerchToString() + "\n");
                    bWriter.close();
                    fWriter.close();
                }
            }
        }

        for (int i = 0; i < parserResults.size(); i++) {
            FileReader fReader = new FileReader("src/main/resources/util/ParserResults.txt");
            BufferedReader bReader = new BufferedReader(fReader);

            String line;
            Boolean match = false;
            while ((line = bReader.readLine()) != null) {
                if (line.equals(parserResults.get(i).parserToString())) {
                    match = true;
                }
            }
            bReader.close();

            if (!match) {
                File file = new File("src/main/resources/util/ParserResults.txt");
                FileWriter fWriter = new FileWriter(file.getAbsoluteFile(), true);
                BufferedWriter bWriter = new BufferedWriter(fWriter);
                bWriter.write(parserResults.get(i).parserToString() + "\n");
                bWriter.close();
                fWriter.close();
            }
        }
    }
}