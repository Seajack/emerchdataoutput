/**
 * Created by Mad on 0006, 06-02-17.
 */
public class ParserDataContainer {

    private String ebNo;
    private String clientID;

    public ParserDataContainer(String data1, String data2) {
        ebNo = data1;
        clientID = data2;
    }

    public ParserDataContainer(String parserData) {
        String[] data = parserData.split(",");

        ebNo = data[0];
        clientID = data[1];
    }

    public String parserToString() {
        return ebNo + "," + clientID;
    }

    public String getClientID() {
        return clientID;
    }

    public void setClientID(String clientID) {
        this.clientID = clientID;
    }

    public String getEbNo() {
        return ebNo;
    }

    public void setEbNo(String ebNo) {
        this.ebNo = ebNo;
    }
}
