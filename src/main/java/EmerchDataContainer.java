/**
 * Created by Mad on 0006, 06-02-17.
 */
public class EmerchDataContainer {

    private String customerNo;
    private String tradingName;
    private String abn;
    private String merchantID;
    private String settlementAccName;
    private String settlementBSB;
    private String settlementAccID;
    private String billingAccName;
    private String billingBSB;
    private String billingAccID;
    private String establishmentFee;
    private String monthlyAccessFee;
    private String crTransactionFeePBP;
    private String drTransactionFeePBP;
    private String crTransactionFeePBW;
    private String drTransactionFeePBW;
    private String transactionFeeVT_B_SP;
    private String crTransactionFee3DS;
    private String transactionFeePIP;
    private String msfCreditPIP;
    private String msfAmexPIP;
    private String dishonourFeePIP;
    private String chargebackFeePIP;
    private String ddRegistrationFee;
    private String transactionFeeAEFT;
    private String transactionFeeAEFTBranch;
    private String dishonourFeeAEFT;
    private String contactName;
    private String contactPhone1;
    private String contactPhone2;
    private String faxNo;
    private String emailAddress;
    private String websiteAddress;
    private String mcc;
    private String threeDS;
    private String ebNo;
    private String terminalID;
    private String productType;
    private String amexNo;
    private String dinersNo;
    private String linkedClientID;
    private String sameSettlementAccount;
    private String bulkSettlementRequired;
    private String bPayID;
    private String lockedBoxID;
    private String eftposID;
    private String directEntryID;
    private String directEntryTracingAccName;
    private String directentryTracingBSB;
    private String directEntryTracingAccID;
    private String directEntrySettlementAccName;
    private String directentrySettlementAccBSB;
    private String directEntrySettlementAccID;
    private String directEntryBillingAccName;
    private String directEntryBillingBSB;
    private String directEntryBillingAccID;
    private String pipID;
    private String pipSettlementAccName;
    private String pipSettlementBSB;
    private String pipSettlementAccID;
    private String pipBillingAccName;
    private String pipBillingBSB;
    private String pipBillingAccID;
    private String offSystemBSB;
    private String offSystemAccount;
    private String mailingName;
    private String mailingAddress1;
    private String mailingAddress2;
    private String mailingAddress3;
    private String mailingAddress4;
    private String bankerName;
    private String bankerPhone;
    private String bankerEmail;
    private String bankerBSB;
    private String clientID;
    private String setupDate;
    private String notes;
    private String setupBy;


    public EmerchDataContainer(String eMerchData) {

        String[] data = eMerchData.split(",", 78);

        customerNo = data[0];
        tradingName = data[1];
        abn = data[2];
        merchantID = data[3];
        settlementAccName = data[4];
        settlementBSB = data[5];
        settlementAccID = data[6];
        billingAccName = data[7];
        billingBSB = data[8];
        billingAccID = data[9];
        establishmentFee = data[10];
        monthlyAccessFee = data[11];
        crTransactionFeePBP = data[12];
        drTransactionFeePBP = data[13];
        crTransactionFeePBW = data[14];
        drTransactionFeePBW = data[15];
        transactionFeeVT_B_SP = data[16];
        crTransactionFee3DS = data[17];
        transactionFeePIP = data[18];
        msfCreditPIP = data[19];
        msfAmexPIP = data[20];
        dishonourFeePIP = data[21];
        chargebackFeePIP = data[22];
        ddRegistrationFee = data[23];
        transactionFeeAEFT = data[24];
        transactionFeeAEFTBranch = data[25];
        dishonourFeeAEFT = data[26];
        contactName = data[27];
        contactPhone1 = data[28];
        contactPhone2 = data[29];
        faxNo = data[30];
        emailAddress = data[31];
        websiteAddress = data[32];
        mcc = data[33];
        threeDS = data[34];
        ebNo = data[35];
        terminalID = data[36];
        productType = data[37];
        amexNo = data[38];
        dinersNo = data[39];
        linkedClientID = data[40];
        sameSettlementAccount = data[41];
        bulkSettlementRequired = data[42];
        bPayID = data[43];
        lockedBoxID = data[44];
        eftposID = data[45];
        directEntryID = data[46];
        directEntryTracingAccName = data[47];
        directentryTracingBSB = data[48];
        directEntryTracingAccID = data[49];
        directEntrySettlementAccName = data[50];
        directentrySettlementAccBSB = data[51];
        directEntrySettlementAccID = data[52];
        directEntryBillingAccName = data[53];
        directEntryBillingBSB = data[54];
        directEntryBillingAccID = data[55];
        pipID = data[56];
        pipSettlementAccName = data[57];
        pipSettlementBSB = data[58];
        pipSettlementAccID = data[59];
        pipBillingAccName = data[60];
        pipBillingBSB = data[61];
        pipBillingAccID = data[62];
        offSystemBSB = data[63];
        offSystemAccount = data[64];
        mailingName = data[65];
        mailingAddress1 = data[66];
        mailingAddress2 = data[67];
        mailingAddress3 = data[68];
        mailingAddress4 = data[69];
        bankerName = data[70];
        bankerPhone = data[71];
        bankerEmail = data[72];
        bankerBSB = data[73];
        clientID = data[74];
        setupDate = data[75];
        notes = data[76];
        setupBy = data[77];


        notes.concat(" - AutoImport");
    }

    public String eMerchToString() {
        return customerNo + "," + tradingName + "," + abn + "," + merchantID + "," + settlementAccName + "," + settlementBSB + ","
                + settlementAccID + "," + billingAccName + "," + billingBSB + "," + billingAccID + "," + establishmentFee + ","
                + monthlyAccessFee + "," + crTransactionFeePBP + "," + drTransactionFeePBP + "," + crTransactionFeePBW + ","
                + drTransactionFeePBW + "," + transactionFeeVT_B_SP + "," + crTransactionFee3DS + "," + transactionFeePIP + ","
                + msfCreditPIP + "," + msfAmexPIP + "," + dishonourFeePIP + "," + chargebackFeePIP + "," + ddRegistrationFee + ","
                + transactionFeeAEFT + "," + transactionFeeAEFTBranch + "," + dishonourFeeAEFT + "," + contactName + ","
                + contactPhone1 + "," + contactPhone2 + "," + faxNo + "," + emailAddress + "," + websiteAddress + "," + mcc + ","
                + threeDS + "," + ebNo + "," + terminalID + "," + productType + "," + amexNo + "," + dinersNo + "," + linkedClientID + ","
                + sameSettlementAccount + "," + bulkSettlementRequired + "," + bPayID + "," + lockedBoxID + "," + eftposID + ","
                + directEntryID + "," + directEntryTracingAccName + "," + directentryTracingBSB + "," + directEntryTracingAccID + ","
                + directEntrySettlementAccName + "," + directentrySettlementAccBSB + "," + directEntrySettlementAccID + ","
                + directEntryBillingAccName + "," + directEntryBillingBSB + "," + directEntryBillingAccID + "," + pipID + ","
                + pipSettlementAccName + "," + pipSettlementBSB + "," + pipSettlementAccID + "," + pipBillingAccName + ","
                + pipBillingBSB + "," + pipBillingAccID + "," + offSystemBSB + "," + offSystemAccount + "," + mailingName + ","
                + mailingAddress1 + "," + mailingAddress2 + "," + mailingAddress3 + "," + mailingAddress4 + "," + bankerName + ","
                + bankerPhone + "," + bankerEmail + "," + bankerBSB + "," + clientID + "," + setupDate + "," + notes + "," + setupBy;
    }

    public String getCustomerNo() {
        return customerNo;
    }

    public void setCustomerNo(String customerNo) {
        this.customerNo = customerNo;
    }

    public String getTradingName() {
        return tradingName;
    }

    public void setTradingName(String tradingName) {
        this.tradingName = tradingName;
    }

    public String getAbn() {
        return abn;
    }

    public void setAbn(String abn) {
        this.abn = abn;
    }

    public String getMerchantID() {
        return merchantID;
    }

    public void setMerchantID(String merchantID) {
        this.merchantID = merchantID;
    }

    public String getSettlementAccName() {
        return settlementAccName;
    }

    public void setSettlementAccName(String settlementAccName) {
        this.settlementAccName = settlementAccName;
    }

    public String getSettlementBSB() {
        return settlementBSB;
    }

    public void setSettlementBSB(String settlementBSB) {
        this.settlementBSB = settlementBSB;
    }

    public String getSettlementAccID() {
        return settlementAccID;
    }

    public void setSettlementAccID(String settlementAccID) {
        this.settlementAccID = settlementAccID;
    }

    public String getBillingAccName() {
        return billingAccName;
    }

    public void setBillingAccName(String billingAccName) {
        this.billingAccName = billingAccName;
    }

    public String getBillingBSB() {
        return billingBSB;
    }

    public void setBillingBSB(String billingBSB) {
        this.billingBSB = billingBSB;
    }

    public String getBillingAccID() {
        return billingAccID;
    }

    public void setBillingAccID(String billingAccID) {
        this.billingAccID = billingAccID;
    }

    public String getEstablishmentFee() {
        return establishmentFee;
    }

    public void setEstablishmentFee(String establishmentFee) {
        this.establishmentFee = establishmentFee;
    }

    public String getMonthlyAccessFee() {
        return monthlyAccessFee;
    }

    public void setMonthlyAccessFee(String monthlyAccessFee) {
        this.monthlyAccessFee = monthlyAccessFee;
    }

    public String getCrTransactionFeePBP() {
        return crTransactionFeePBP;
    }

    public void setCrTransactionFeePBP(String crTransactionFeePBP) {
        this.crTransactionFeePBP = crTransactionFeePBP;
    }

    public String getDrTransactionFeePBP() {
        return drTransactionFeePBP;
    }

    public void setDrTransactionFeePBP(String drTransactionFeePBP) {
        this.drTransactionFeePBP = drTransactionFeePBP;
    }

    public String getCrTransactionFeePBW() {
        return crTransactionFeePBW;
    }

    public void setCrTransactionFeePBW(String crTransactionFeePBW) {
        this.crTransactionFeePBW = crTransactionFeePBW;
    }

    public String getDrTransactionFeePBW() {
        return drTransactionFeePBW;
    }

    public void setDrTransactionFeePBW(String drTransactionFeePBW) {
        this.drTransactionFeePBW = drTransactionFeePBW;
    }

    public String getTransactionFeeVT_B_SP() {
        return transactionFeeVT_B_SP;
    }

    public void setTransactionFeeVT_B_SP(String transactionFeeVT_B_SP) {
        this.transactionFeeVT_B_SP = transactionFeeVT_B_SP;
    }

    public String getCrTransactionFee3DS() {
        return crTransactionFee3DS;
    }

    public void setCrTransactionFee3DS(String crTransactionFee3DS) {
        this.crTransactionFee3DS = crTransactionFee3DS;
    }

    public String getTransactionFeePIP() {
        return transactionFeePIP;
    }

    public void setTransactionFeePIP(String transactionFeePIP) {
        this.transactionFeePIP = transactionFeePIP;
    }

    public String getMsfCreditPIP() {
        return msfCreditPIP;
    }

    public void setMsfCreditPIP(String msfCreditPIP) {
        this.msfCreditPIP = msfCreditPIP;
    }

    public String getMsfAmexPIP() {
        return msfAmexPIP;
    }

    public void setMsfAmexPIP(String msfAmexPIP) {
        this.msfAmexPIP = msfAmexPIP;
    }

    public String getDishonourFeePIP() {
        return dishonourFeePIP;
    }

    public void setDishonourFeePIP(String dishonourFeePIP) {
        this.dishonourFeePIP = dishonourFeePIP;
    }

    public String getChargebackFeePIP() {
        return chargebackFeePIP;
    }

    public void setChargebackFeePIP(String chargebackFeePIP) {
        this.chargebackFeePIP = chargebackFeePIP;
    }

    public String getDdRegistrationFee() {
        return ddRegistrationFee;
    }

    public void setDdRegistrationFee(String ddRegistrationFee) {
        this.ddRegistrationFee = ddRegistrationFee;
    }

    public String getTransactionFeeAEFT() {
        return transactionFeeAEFT;
    }

    public void setTransactionFeeAEFT(String transactionFeeAEFT) {
        this.transactionFeeAEFT = transactionFeeAEFT;
    }

    public String getTransactionFeeAEFTBranch() {
        return transactionFeeAEFTBranch;
    }

    public void setTransactionFeeAEFTBranch(String transactionFeeAEFTBranch) {
        this.transactionFeeAEFTBranch = transactionFeeAEFTBranch;
    }

    public String getDishonourFeeAEFT() {
        return dishonourFeeAEFT;
    }

    public void setDishonourFeeAEFT(String dishonourFeeAEFT) {
        this.dishonourFeeAEFT = dishonourFeeAEFT;
    }

    public String getContactName() {
        return contactName;
    }

    public void setContactName(String contactName) {
        this.contactName = contactName;
    }

    public String getContactPhone1() {
        return contactPhone1;
    }

    public void setContactPhone1(String contactPhone1) {
        this.contactPhone1 = contactPhone1;
    }

    public String getContactPhone2() {
        return contactPhone2;
    }

    public void setContactPhone2(String contactPhone2) {
        this.contactPhone2 = contactPhone2;
    }

    public String getFaxNo() {
        return faxNo;
    }

    public void setFaxNo(String faxNo) {
        this.faxNo = faxNo;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public String getWebsiteAddress() {
        return websiteAddress;
    }

    public void setWebsiteAddress(String websiteAddress) {
        this.websiteAddress = websiteAddress;
    }

    public String getMcc() {
        return mcc;
    }

    public void setMcc(String mcc) {
        this.mcc = mcc;
    }

    public String getThreeDS() {
        return threeDS;
    }

    public void setThreeDS(String threeDS) {
        this.threeDS = threeDS;
    }

    public String getEbNo() {
        return ebNo;
    }

    public void setEbNo(String ebNo) {
        this.ebNo = ebNo;
    }

    public String getTerminalID() {
        return terminalID;
    }

    public void setTerminalID(String terminalID) {
        this.terminalID = terminalID;
    }

    public String getProductType() {
        return productType;
    }

    public void setProductType(String productType) {
        this.productType = productType;
    }

    public String getAmexNo() {
        return amexNo;
    }

    public void setAmexNo(String amexNo) {
        this.amexNo = amexNo;
    }

    public String getDinersNo() {
        return dinersNo;
    }

    public void setDinersNo(String dinersNo) {
        this.dinersNo = dinersNo;
    }

    public String getLinkedClientID() {
        return linkedClientID;
    }

    public void setLinkedClientID(String linkedClientID) {
        this.linkedClientID = linkedClientID;
    }

    public String getSameSettlementAccount() {
        return sameSettlementAccount;
    }

    public void setSameSettlementAccount(String sameSettlementAccount) {
        this.sameSettlementAccount = sameSettlementAccount;
    }

    public String getBulkSettlementRequired() {
        return bulkSettlementRequired;
    }

    public void setBulkSettlementRequired(String bulkSettlementRequired) {
        this.bulkSettlementRequired = bulkSettlementRequired;
    }

    public String getbPayID() {
        return bPayID;
    }

    public void setbPayID(String bPayID) {
        this.bPayID = bPayID;
    }

    public String getLockedBoxID() {
        return lockedBoxID;
    }

    public void setLockedBoxID(String lockedBoxID) {
        this.lockedBoxID = lockedBoxID;
    }

    public String getEftposID() {
        return eftposID;
    }

    public void setEftposID(String eftposID) {
        this.eftposID = eftposID;
    }

    public String getDirectEntryID() {
        return directEntryID;
    }

    public void setDirectEntryID(String directEntryID) {
        this.directEntryID = directEntryID;
    }

    public String getDirectEntryTracingAccName() {
        return directEntryTracingAccName;
    }

    public void setDirectEntryTracingAccName(String directEntryTracingAccName) {
        this.directEntryTracingAccName = directEntryTracingAccName;
    }

    public String getDirectentryTracingBSB() {
        return directentryTracingBSB;
    }

    public void setDirectentryTracingBSB(String directentryTracingBSB) {
        this.directentryTracingBSB = directentryTracingBSB;
    }

    public String getDirectEntryTracingAccID() {
        return directEntryTracingAccID;
    }

    public void setDirectEntryTracingAccID(String directEntryTracingAccID) {
        this.directEntryTracingAccID = directEntryTracingAccID;
    }

    public String getDirectEntrySettlementAccName() {
        return directEntrySettlementAccName;
    }

    public void setDirectEntrySettlementAccName(String directEntrySettlementAccName) {
        this.directEntrySettlementAccName = directEntrySettlementAccName;
    }

    public String getDirectentrySettlementAccBSB() {
        return directentrySettlementAccBSB;
    }

    public void setDirectentrySettlementAccBSB(String directentrySettlementAccBSB) {
        this.directentrySettlementAccBSB = directentrySettlementAccBSB;
    }

    public String getDirectEntrySettlementAccID() {
        return directEntrySettlementAccID;
    }

    public void setDirectEntrySettlementAccID(String directEntrySettlementAccID) {
        this.directEntrySettlementAccID = directEntrySettlementAccID;
    }

    public String getDirectEntryBillingAccName() {
        return directEntryBillingAccName;
    }

    public void setDirectEntryBillingAccName(String directEntryBillingAccName) {
        this.directEntryBillingAccName = directEntryBillingAccName;
    }

    public String getDirectEntryBillingBSB() {
        return directEntryBillingBSB;
    }

    public void setDirectEntryBillingBSB(String directEntryBillingBSB) {
        this.directEntryBillingBSB = directEntryBillingBSB;
    }

    public String getDirectEntryBillingAccID() {
        return directEntryBillingAccID;
    }

    public void setDirectEntryBillingAccID(String directEntryBillingAccID) {
        this.directEntryBillingAccID = directEntryBillingAccID;
    }

    public String getPipID() {
        return pipID;
    }

    public void setPipID(String pipID) {
        this.pipID = pipID;
    }

    public String getPipSettlementAccName() {
        return pipSettlementAccName;
    }

    public void setPipSettlementAccName(String pipSettlementAccName) {
        this.pipSettlementAccName = pipSettlementAccName;
    }

    public String getPipSettlementBSB() {
        return pipSettlementBSB;
    }

    public void setPipSettlementBSB(String pipSettlementBSB) {
        this.pipSettlementBSB = pipSettlementBSB;
    }

    public String getPipSettlementAccID() {
        return pipSettlementAccID;
    }

    public void setPipSettlementAccID(String pipSettlementAccID) {
        this.pipSettlementAccID = pipSettlementAccID;
    }

    public String getPipBillingAccName() {
        return pipBillingAccName;
    }

    public void setPipBillingAccName(String pipBillingAccName) {
        this.pipBillingAccName = pipBillingAccName;
    }

    public String getPipBillingBSB() {
        return pipBillingBSB;
    }

    public void setPipBillingBSB(String pipBillingBSB) {
        this.pipBillingBSB = pipBillingBSB;
    }

    public String getPipBillingAccID() {
        return pipBillingAccID;
    }

    public void setPipBillingAccID(String pipBillingAccID) {
        this.pipBillingAccID = pipBillingAccID;
    }

    public String getOffSystemBSB() {
        return offSystemBSB;
    }

    public void setOffSystemBSB(String offSystemBSB) {
        this.offSystemBSB = offSystemBSB;
    }

    public String getOffSystemAccount() {
        return offSystemAccount;
    }

    public void setOffSystemAccount(String offSystemAccount) {
        this.offSystemAccount = offSystemAccount;
    }

    public String getMailingName() {
        return mailingName;
    }

    public void setMailingName(String mailingName) {
        this.mailingName = mailingName;
    }

    public String getMailingAddress1() {
        return mailingAddress1;
    }

    public void setMailingAddress1(String mailingAddress1) {
        this.mailingAddress1 = mailingAddress1;
    }

    public String getMailingAddress2() {
        return mailingAddress2;
    }

    public void setMailingAddress2(String mailingAddress2) {
        this.mailingAddress2 = mailingAddress2;
    }

    public String getMailingAddress3() {
        return mailingAddress3;
    }

    public void setMailingAddress3(String mailingAddress3) {
        this.mailingAddress3 = mailingAddress3;
    }

    public String getMailingAddress4() {
        return mailingAddress4;
    }

    public void setMailingAddress4(String mailingAddress4) {
        this.mailingAddress4 = mailingAddress4;
    }

    public String getBankerName() {
        return bankerName;
    }

    public void setBankerName(String bankerName) {
        this.bankerName = bankerName;
    }

    public String getBankerPhone() {
        return bankerPhone;
    }

    public void setBankerPhone(String bankerPhone) {
        this.bankerPhone = bankerPhone;
    }

    public String getBankerEmail() {
        return bankerEmail;
    }

    public void setBankerEmail(String bankerEmail) {
        this.bankerEmail = bankerEmail;
    }

    public String getBankerBSB() {
        return bankerBSB;
    }

    public void setBankerBSB(String bankerBSB) {
        this.bankerBSB = bankerBSB;
    }

    public String getSetupBy() {
        return setupBy;
    }

    public void setSetupBy(String setupBy) {
        this.setupBy = setupBy;
    }

    public String getSetupDate() {
        return setupDate;
    }

    public void setSetupDate(String setupDate) {
        this.setupDate = setupDate;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getClientID() {
        return clientID;
    }

    public void setClientID(String clientID) {
        this.clientID = clientID;
    }

//    String data1, String data2, String data3, String data4, String data5, String data6, String data7,
//    String data8, String data9, String data10, String data11, String data12, String data13,
//    String data14, String data15, String data16, String data17, String data18, String data19,
//    String data20, String data21, String data22, String data23, String data24, String data25,
//    String data26, String data27, String data28, String data29, String data30, String data31,
//    String data32, String data33, String data34, String data35, String data36, String data37,
//    String data38, String data39, String data40, String data41, String data42, String data43,
//    String data44, String data45, String data46, String data47, String data48, String data49,
//    String data50, String data51, String data52, String data53, String data54, String data55,
//    String data56, String data57, String data58, String data59, String data60, String data61,
//    String data62, String data63, String data64, String data65, String data66, String data67,
//    String data68, String data69, String data70, String data71, String data72, String data73,
//    String data74, String data75, String data76
}