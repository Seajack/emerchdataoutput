import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * Created by Mad on 0020, 20-09-18.
 */
public class PropertiesContainer {

    private String fileName;
    private Properties prop;
    InputStream inputStream;

    public PropertiesContainer(String fileName) throws IOException{
        this.fileName = fileName;
        prop = new Properties();

        loadPropValues();
    }

    public void loadPropValues() throws IOException {
            inputStream = getClass().getClassLoader().getResourceAsStream(fileName);

            if(inputStream != null) {
                prop.load(inputStream);
            } else {
                throw new FileNotFoundException("Properties file '" + fileName + "' not found.");
            }

            inputStream.close();
    }

    public String getVar(String varName) throws IOException{
        String varValue = prop.getProperty(varName);

        if(varValue != null) {
            return varValue;
        } else {
            throw new IOException("varName does not match properties file contents");
        }
    }
}
