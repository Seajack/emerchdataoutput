import java.util.ArrayList;

/**
 * Created by Mad on 0014, 14-05-17.
 */
public class DataContainer {
    private ArrayList<ParserDataContainer> parserResults = new ArrayList();
    private ArrayList<EmerchDataContainer> eMerchImports = new ArrayList();

    public DataContainer(ArrayList<ParserDataContainer> parserList, ArrayList<EmerchDataContainer> eMerchList) {
        parserResults = parserList;
        eMerchImports = eMerchList;
    }

    public ArrayList<ParserDataContainer> getParserResults() {
        return parserResults;
    }

    public ArrayList<EmerchDataContainer> geteMerchImports() {
        return eMerchImports;
    }
}
