import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by Mad on 0014, 14-05-17.
 */
public class InitializeHandler {
    private ArrayList<ParserDataContainer> parserResults = new ArrayList();
    private ArrayList<EmerchDataContainer> eMerchImports = new ArrayList();
    DataContainer container = new DataContainer(parserResults, eMerchImports);

    public InitializeHandler() throws IOException {

    }

    public void Initialize() throws IOException {
        InitializeExports();
        InitializeParsers();
    }

    public void InitializeExports() throws IOException {
        FileReader fReader = new FileReader("src/main/resources/util/eMerchExports.txt");
        BufferedReader bReader = new BufferedReader(fReader);

        String line;
        while ((line = bReader.readLine()) != null) {
            EmerchDataContainer eMerchData = new EmerchDataContainer(line);
            eMerchImports.add(eMerchData);

        }
        bReader.close();
    }

    public void InitializeParsers() throws IOException {
        FileReader fReader = new FileReader("src/main/resources/util/ParserResults.txt");
        BufferedReader bReader = new BufferedReader(fReader);

        String line;
        while ((line = bReader.readLine()) != null) {
            ParserDataContainer parserData = new ParserDataContainer(line);
            parserResults.add(parserData);
        }
        bReader.close();
    }

    public DataContainer getDataContainer() {
        return container;
    }
}
